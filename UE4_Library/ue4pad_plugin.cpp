// ue4pad_plugin.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

const char address[] = "192.168.127.92";
const char msg[] = "ciao";
char buffer[1024];
int theta = 0;
int speed = 0;

int _tmain(int argc, _TCHAR* argv[])
{
    WSADATA wsaData;
    WORD version MAKEWORD(2, 0);
    
    WSAStartup(version, &wsaData);

    SOCKET client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    SOCKADDR_IN servAddr;
    ZeroMemory(&servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.S_un.S_addr = inet_addr(address);
    servAddr.sin_port = htons(54321);

    int ret = connect(client, (LPSOCKADDR)&servAddr, sizeof(struct sockaddr));
    if (ret == SOCKET_ERROR)
    {
        printf("connect failed\n");
    }
    else
    {
        printf("connect success\n");
        send(client, msg, sizeof(msg), 0);

        int bytes;
        do
        {
            ZeroMemory(buffer, sizeof(buffer));
            bytes = recv(client, buffer, sizeof(buffer), 0);

            char* token = strtok(buffer, "*");
            while (token != NULL)
            {
                char* token2 = strtok(buffer, "|");
                theta = atoi(token2);
                token2 = strtok(NULL, "|");
                speed = atoi(token2);
                token = strtok(NULL, "*");
                printf("theta=%d speed=%d\n", theta, speed);
            }
        } while (bytes > 0);
    }
    system("pause");
	return 0;
}

