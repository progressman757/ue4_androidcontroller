package it.omidev.ue4droidpad;

import java.util.LinkedList;
import java.util.Queue;

public class MessageQueue
{
	private Queue<String> messages;
	private Object lock;

	public MessageQueue()
	{
		messages = new LinkedList<String>();
		lock = new Object();
	}

	public void cleanUp()
	{
		messages.clear();
	}

	public boolean isEmpty()
	{
		return messages.size() == 0;
	}

	public void postMessage(String msg)
	{
		synchronized(lock)
		{
			messages.add(msg);
			if(messages.size() > 5)
				messages.poll();
			lock.notifyAll();
		}
	}

	public String getMessage()
	{
		String ret = null;
		synchronized(lock)
		{
			if(messages.isEmpty())
			{
				try
				{
					lock.wait();
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
					return null;
				}
			}
			ret = messages.poll();
		}
		return ret;
	}

}
